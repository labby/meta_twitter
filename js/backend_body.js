/**
 * @module          Meta Twitter
 * @author          cms-lab
 * @copyright       2017-2023 cms-lab
 * @link            https://cms-lab.com
 * @license         custom license: https://cms-lab.com/_documentation/meta-twitter/license.php
 * @license_terms   please see license
 *
 */


$('.ui.dropdown')
  .dropdown()
;

$('.ui.checkbox')
  .checkbox()
;

