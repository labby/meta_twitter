<?php

/**
 * @module          Meta Twitter
 * @author          cms-lab
 * @copyright       2017-2023 cms-lab
 * @link            https://cms-lab.com
 * @license         custom license: https://cms-lab.com/_documentation/meta-twitter/license.php
 * @license_terms   please see license
 *
 */

// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;   
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include secure file

$FORWARD = "Location: ".ADMIN_URL."/admintools/tool.php?tool=meta_twitter&leptoken=".get_leptoken();

if(isset($_POST['cancel']) ) {
	header( $FORWARD );
	exit();
}

// no id
if(!isset($_POST['save_settings']) || ($_POST['save_settings'] == '') ) {
	header( $FORWARD );
	exit();
}

$admin = new LEPTON_admin('admintools', 'admintools');


// save settings
if(isset ($_POST['job']) && ($_POST['job'] == 'save_settings') ) {

	//	save elements
	$request = new LEPTON_request();	

	$all_names = array (
		'card'			=> array ('type' => 'string',	'default' => '',	'range' =>""),
		'site'			=> array ('type' => 'string',	'default' => '',	'range' =>""),		
		'creator'		=> array ('type' => 'string',	'default' => '',	'range' =>""),		
		'default_image'	=> array ('type' => 'string',	'default' => '',	'range' =>"")
	);

	$all_values = array ();

	foreach($all_names as $item=>$options)
	{
		$all_values[$item] = $request->get_request($item, $options['default'], $options['type'], $options['range']);
	}
	
	$table = TABLE_PREFIX."mod_meta_twitter_settings";
	$query = "UPDATE `" . $table . "` SET ";
	
	foreach($all_values as $key =>$value) 
	{
		$query .= "`" . $key . "`='".$value."', ";
	}
	
	$query = substr($query, 0, -2)."  WHERE id=".$_POST['save_settings'];
	
	$result = $database->simple_query( $query );
			
	$admin->print_success('record_ saved', ADMIN_URL."/admintools/tool.php?tool=meta_twitter");
}
