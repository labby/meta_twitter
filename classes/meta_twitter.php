<?php

/**
 * @module          Meta Twitter
 * @author          cms-lab
 * @copyright       2017-2023 cms-lab
 * @link            https://cms-lab.com
 * @license         custom license: https://cms-lab.com/_documentation/meta-twitter/license.php
 * @license_terms   please see license
 *
 */

class meta_twitter extends LEPTON_abstract
{
	public array $settings = [];	
	public array $current_page = [];
	public string $image_url = '';	
	public string $addon_color = 'blue';
	public string $action = LEPTON_URL.'/modules/meta_twitter/';	
	public string $action_url = ADMIN_URL . '/admintools/tool.php?tool=meta_twitter';	

	public object|null $oTwig = null;
	public LEPTON_database $database;
	static $instance;

	public function initialize() 
	{
		$this->database = LEPTON_database::getInstance();	
		$this->oTwig = lib_twig_box::getInstance();
		$this->oTwig->registerModule('meta_twitter');		
		$this->init_tool();
	}
	
	public function init_tool( $sToolname = '' )
	{
		if(!defined('PAGE_ID')) {
			define ('PAGE_ID','0');
		}

		//get current_page
		$this->database->execute_query(
			"SELECT * FROM ".TABLE_PREFIX."pages WHERE page_id = ".(PAGE_ID ?? 0),
			true,
			$this->current_page,
			false
		); 
		
		//get settings
		$this->database->execute_query(
			"SELECT * FROM ".TABLE_PREFIX."mod_meta_twitter_settings " ,
			true,
			$this->settings,
			false
		);	


		//get image_url with biggest width
		//get all sections
		$all_sections = [];
		$this->database->execute_query(
			"SELECT `content` FROM ".TABLE_PREFIX."mod_wysiwyg WHERE page_id = ".(PAGE_ID ?? 0),
			true,
			$all_sections,
			true
		);	
		
		$biggest_width = 0;
		$origImageSrc = [];
		foreach ($all_sections as $temp_section	)
		{
			$htmlContent = $temp_section['content'];	
		
			// read all image tags into an array
			preg_match_all('/<img[^>]+>/i',$htmlContent, $imgTags); 

			for ($i = 0; $i < count($imgTags[0]); $i++) 
			{
			  // get the source string
			  preg_match('/src="([^"]+)/i',$imgTags[0][$i], $imgage);

			  // remove opening 'src=' tag, can`t get the regex right
			  $origImageSrc[] = str_ireplace( 'src="', '',  $imgage[0]);
			}
			if (!empty($origImageSrc))	{	
				foreach($origImageSrc as $temp_image) 
				{
					$info =   getimagesize( $temp_image );
					if($info[0]> $biggest_width) 
					{
						$biggest_width = $info[0];
						$this->image_url = $temp_image;
					}
				}
			}
		}		
	}
	
	public function get_settings() 
	{
		// data for twig template engine	
		$data = array(
			'oMTF'			=> $this,
			'readme_link'	=> 'https://cms-lab.com/_documentation/meta-twitter/readme.php',
			'leptoken'		=> get_leptoken()			
			);

		// get the template-engine	
		echo $this->oTwig->render(
			"@meta_twitter/settings.lte",	//	template-filename
			$data						//	template-data
		);		
		
	}	

	public function show_info() 
	{
		// create links
		$support_link = "<a href=\"#\">NO Live-Support / FAQ</a>";	
		$readme_link = "<a href=\"https://cms-lab.com/_documentation/meta-twitter/readme.php \" class=\"info\" target=\"_blank\">Readme</a>";	

		// data for twig template engine	
		$data = array(
			'oMTF'			=> $this,
			'readme_link'	=> $readme_link,		
			'SUPPORT'		=> $support_link,	
			'image_url'		=> 'https://cms-lab.com/_documentation/media/meta_twitter/twitter.jpg'
			);

		// get the template-engine	
		echo $this->oTwig->render(
			"@meta_twitter/info.lte",	//	template-filename
			$data						//	template-data
		);		
		
	}
	
	
	
	public function build_tags()
	{
		
		// data for twig template engine	
		$data = array(
			'oMTF'			=> $this,
			'default_language'	=> DEFAULT_LANGUAGE,
			'website_description'	=> WEBSITE_DESCRIPTION
        );

		// get the template-engine	
		return $this->oTwig->render( 
			"@meta_twitter/output.lte",	//	template-filename
			$data						//	template-data
		);	

	}	

} // end of class
