<?php

/**
 * @module          Meta Twitter
 * @author          cms-lab
 * @copyright       2017-2023 cms-lab
 * @link            https://cms-lab.com
 * @license         custom license: https://cms-lab.com/_documentation/meta-twitter/license.php
 * @license_terms   please see license
 *
 */

// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;   
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include secure file

$MOD_META_TWITTER = [
	'action'			=> "Action",
	'add_form'			=> "Add Form",
	'html_footer'		=> "HTML Footer",	
	'modify'			=> "modify",
	'preview'			=> "preview",		
	'edit'				=> "Edit",
	'copy'				=> "copy",	
	'details'			=> "Details",
	'name'				=> "Name",
	'value'				=> "Value",
	'info'				=> "Addon Info",		

	//	messages
	'record_deleted'	=> "Datensatz wurde gelöscht",	
	'record_saved'		=> "Datensatz wurde gespeichert",
	'record_not_saved'	=> "Datensatz wurde nicht gespeichert, da doppelte Werte vorhanden sind!",	
	'record_inserted'	=> "Datensatz wurde hinzugefügt"		
];
