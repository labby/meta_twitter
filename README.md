### Meta-Twitter
============

A LEPTON tool to set Twitter meta tags.

#### Requirements

* [LEPTON CMS][1], Version dee precheck.php


#### Installation

* download latest [.zip][2] installation archive
* in CMS backend select the file from "Add-ons" -> "Modules" -> "Install module"

#### Notice

After installing addon you are done. <br />
Please go to "Admintools" in the backend and use it!

For further informations please read [the readme file][3]


[1]: https://lepton-cms.org "LEPTON CMS"
[2]: http://www.lepton-cms.com/lepador/admintools/meta-twitter.php
[3]: https://cms-lab.com/_documentation/meta-twitter/readme.php
