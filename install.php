<?php

/**
 * @module          Meta Twitter
 * @author          cms-lab
 * @copyright       2017-2023 cms-lab
 * @link            https://cms-lab.com
 * @license         custom license: https://cms-lab.com/_documentation/meta-twitter/license.php
 * @license_terms   please see license
 *
 */

// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;   
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include secure file

// mod_meta_twitter
$table_fields="
	  `id` INT NOT NULL AUTO_INCREMENT,
	  `card` varchar(32) NOT NULL DEFAULT 'summary_large_image',
	  `default_image` varchar(256) NOT NULL DEFAULT 'https://cms-lab.com/_documentation/media/meta_twitter/twitter.jpg',
	  `creator` varchar(64) NOT NULL DEFAULT 'channel',
	  `site` varchar(128) NOT NULL DEFAULT 'website',
	  PRIMARY KEY ( `id` )
	";
LEPTON_handle::install_table("mod_meta_twitter_settings", $table_fields);

// insert some default values
$field_values="
(NULL, 'summary_large_image', 'https://cms-lab.com/_documentation/media/meta_twitter/twitter.jpg', 'channel', 'website')
";
LEPTON_handle::insert_values("mod_meta_twitter_settings", $field_values);

// import default droplets
LEPTON_handle::install_droplets('meta_twitter', 'droplet_meta-twitter');
